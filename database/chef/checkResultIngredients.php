<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<title>Select Information</title>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<link rel="stylesheet" type="text/css" href="style.css" />
</head>

<body>

	<div id="page-wrap">
				
		<div id="contact-area">
			
				<?php
					
					include ('../connection.php');
					
					$menu = $_GET["Menu"];
					$con = makeConnection();
							$sql = 'SELECT DISTINCT (ingridient_name), quantity FROM all_ingridients WHERE description ="' .$menu .'"';
							
							$result = $con->query($sql);

							if(!$result){
								die('Error2: '.mysql_error());
							}
							echo "<table border='1'>
							<caption>".$menu ."</caption>
								<tr>
									<th>Ingrediens</th>
									<th>Quantity</th>		
								</tr>";		
						while($row = $result->fetch_array()){
							echo "<tr>";
							echo "<td>" .$row['ingridient_name']."</td>";
							echo "<td>" .$row['quantity']."</td>";
							echo "</tr>";
						}
						$result->close();
						$con->close();
					
				?>
						
		</div>
	
	</div>

</body>

</html>