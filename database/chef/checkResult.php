<?php 
ob_start();
session_start();
$_SESSION['location'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<title>Select Information</title>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<link rel="stylesheet" type="text/css" href="style.css" />
</head>

<body>

	<div id="page-wrap">
				
		<div id="contact-area">
			
				<?php
					
					include ('../connection.php');
					
					$ssn = $_GET["SSN"];
					$name = $_GET["name"];
					$family = $_GET["family"];
					$hire = $_GET["hire"];
					$button = $_GET["action"];
					$position = $_GET["Position"];
					$user = $_SESSION['username'];
					
					if($position == 'Cook'){
						$pos = 9;
						$sal = 5;
					}
					if($position == 'Dishwasher'){
						$pos = 10;
						$sal = 6;
					}
					
						
					
					if($button == "insert"){
						insertStaff($ssn, $name, $family, $hire, $user, $pos, $sal);
					}elseif($button == "delete"){
						deleteStaff($ssn, $user);
					}elseif($button == "view"){
						viewStaff($user, $pos);
					}
					
					function viewStaff($user, $pos){
						$con = makeConnection();
						$loc_sql = 'SELECT location FROM employee WHERE ssn ='.$user ;
						$res = $con->query($loc_sql);
						$location = $res->fetch_array();
						
						$_SESSION['location']= $location['location'];
						
						$sql_view = 'SELECT ssn, CONCAT(fname,' .'" "'  .',lname) as name, hire_date FROM employee, works_as WHERE ssn=employee_id AND 
							position_id =' .$pos .'  AND location='.$location['location'] .' ORDER BY hire_date DESC';
						
						$result = $con->query($sql_view);
						echo "<table border='1'>
								<tr>
									<th>ssn</th>
									<th>Name</th>
									<th>Hire Date</th>
												
								</tr>";		

						while($row = $result->fetch_array()){
							echo "<tr>";
							echo "<td>" .$row['ssn']."</td>";
							echo "<td>" .$row['name']."</td>";
							echo "<td>" .$row['hire_date']."</td>";
							echo "</tr>";
						}
						$result->close();
						$con->close();
					}
					
					function insertStaff($ssn, $name, $family, $hire, $user, $pos, $sal){
						$con = makeConnection();
						$loc_sql = 'SELECT location FROM employee WHERE ssn ='.$user ;
						$res = $con->query($loc_sql);
						$location = $res->fetch_array();
						
						
						if($ssn != "" && $name != "" && $family != "" && $hire != ""){
							$sql = 'INSERT INTO employee VALUES ('.$ssn .', "' .$name .'", "' .$family.'", "' .$hire .'",' .$location['location'] .')';
							$sql_pos = 'INSERT INTO works_as VALUES (' .$ssn .', "' .$pos .'", "' .$sal .')';
						}
						
						if(!$con->query($sql) && !$con->query($sql_pos)){
							die('Error22: '.mysql_error());
						}else {
							$sql_sel = 'SELECT ssn, CONCAT(fname,' .'" "'  .',lname) as name, hire_date FROM employee, works_as WHERE ssn=employee_id AND 
							(position_id = 9 OR position_id = 11) AND location='.$location['location'] .' ORDER BY hire_date DESC';
						}
					
						$result = $con->query($sql_sel);
						echo "<table border='1'>
								<tr>
									<th>ssn</th>
									<th>Name</th>
									<th>Hire Date</th>
												
								</tr>";		

						while($row = $result->fetch_array()){
							echo "<tr>";
							echo "<td>" .$row['ssn']."</td>";
							echo "<td>" .$row['name']."</td>";
							echo "<td>" .$row['hire_date']."</td>";
							echo "</tr>";
						}
						$result->close();
						$con->close();
					}
					
					function deleteStaff($ssn){
						$con = makeConnection();
						
						if($ssn != ""){
													
							$sql = 'DELETE FROM employee WHERE ssn = '.$ssn;
						}else{
							echo('DELETE FROM employee WHERE ssn = '.$ssn);
						}
				
						$result = $con->query($sql);
						
						if(!$result){
							die('Error2: '.mysql_error());
						}else{
							echo("<h2>The employee $ssn was successufully fired</h2>");
						}
						//$result->close();
						$con->close();
					}
				?>
						
		</div>
	
	</div>

</body>

</html>