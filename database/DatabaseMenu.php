<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

  <head>
    <title> Selection Process </title>
  </head>
  <body>
    <?php
		include ('connection.php');
         
		$menu = $_GET["menu"];
		if($menu == "all_employees"){
			header("Location: /database/staff_table.php");
		}
		if($menu == "all_menus"){
			header("Location: /database/menus.html");
		}
		if($menu == "all_shifts"){
			header("Location: /database/shifts.html");
		}
		if($menu == "all_revenue"){
			header("Location: /database/revenu.php");
		}
	
		if(isset($ssn)&&isset($location)&&isset($position)){
			$ssn = $_GET["SSN"];
			$location = $_GET["Location"];
			$position = $_GET["Position"];
			if($ssn != ""){
				shifts($ssn);
			}elseif($location != "" && $position != ""){
				shifts($location, $position);
			}elseif($location != ""){
				shifts($location);
			}elseif($position != ""){
				shifts($position);
			}
			}
     ?>
  </body>
</html>

