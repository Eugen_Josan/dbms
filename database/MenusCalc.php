<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<title>Select Information</title>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<link rel="stylesheet" type="text/css" href="style.css" />
</head>

<body>

	<div id="page-wrap">
				
		<div id="contact-area">
			
				<?php
					include ('connection.php');
					$loc = $_GET["Location"];
					$menus = $_GET["Menu"];
		
					if($loc == 'Head Office') $location = 1;
					if($loc == 'Hercule Poirot') $location = 2;
					if($loc == 'Bleak House') $location = 3;
					if($loc == 'Spartakus') $location = 4;
					if($loc == 'Sherlock Holmes') $location = 5;
					if($loc == 'Wolf Larsen') $location = 6;
					if($loc == 'Arsene Lupin') $location = 7;
					if($loc == 'Cleopatra') $location = 8;
					if($loc == 'Alice in Wonderland') $location = 9;
					if($loc == 'Through the Looking-Glass') $location = 10;
					if($loc == 'The Three Musketeers') $location = 11;
					if($loc == 'Robinson Crusoe') $location = 12;
					if($loc == 'Don Quixote') $location = 13;
	
					if($loc != "" && $menus != ""){
						menus($location, $menus);
					}elseif($loc != ""){
						menus($loc, 'restaurant_name');
					}elseif($menus != ""){
						menus($menus, 'menu_type');
					}else 
						menus(null, null);
				?>
	
			
				
		</div>
	
	</div>

</body>

</html>