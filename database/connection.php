<?php
	function makeConnection (){
		
		$con = new mysqli("localhost:3306","root" ,"");
					
        mysqli_select_db($con, 'udc353_1'); 

        if(! $con ){
			die('Could not connect: ' . mysql_error());
        }
		return $con;
	}
	function getLocation(){
		$con = makeConnection();
		$user = $_SESSION['username'];
		$loc_sql = 'SELECT location FROM employee WHERE ssn ='.$user ;
		$res = $con->query($loc_sql);
		$location = $res->fetch_array();
		return $location['location'];
	}
	
	function shifts($var1, $var2){
		$con = makeConnection();
		if($var2 != 'ssn' && $var2 != 'position' && $var2 != 'location' && isset($var1)){
			$sql = 'SELECT * FROM all_shifts WHERE location="' .$var1 .'" AND position="' .$var2.'"';
		}elseif (!isset($var1) &&  !isset($var2)){
			$sql = 'SELECT * FROM all_shifts';
		}else{
			$sql = 'SELECT * FROM all_shifts WHERE '.$var2 .' ="' .$var1 .'"';
		}
		$result = $con->query($sql);
		
		if(!$result){
			die('Error2: '.mysql_error());
		}
		
		echo "<table border='1'>
			<tr>
				<th>ssn</th>
				<th>Firstname</th>
				<th>Lastname</th>
				<th>location</th>
				<th>position</th>
				<th>Monday</th>
				<th>Tuesday</th>
				<th>Wednesday</th>
				<th>Thursday</th>
				<th>Friday</th>
				<th>Saturday</th>
				<th>Sunday</th>
				
			</tr>";		
		
		while($row = $result->fetch_array()){
			echo "<tr>";
			echo "<td>" .$row['ssn']."</td>";
			echo "<td>" .$row['fname']."</td>";
			echo "<td>" .$row['lname']."</td>";
			echo "<td>" .$row['location']."</td>";
			echo "<td>" .$row['position']."</td>";
			echo "<td>" .$row['Monday']."</td>";
			echo "<td>" .$row['Tuesday']."</td>";
			echo "<td>" .$row['Wednesday']."</td>";
			echo "<td>" .$row['Thursday']."</td>";
			echo "<td>" .$row['Friday']."</td>";
			echo "<td>" .$row['Saturday']."</td>";
			echo "<td>" .$row['Sunday']."</td>";
			echo "</tr>";
		}
		$result->close();
		$con->close();
	}
	function menus($var1, $var2){
		$con = makeConnection();
		if($var2 != 'menu_type' && isset($var1) && $var2 != 'restaurant_name' ){
			$sql = 'SELECT description, ordinary_price, vip_price  FROM all_menus WHERE location_id="' .$var1 .'" AND menu_type="' .$var2.'"';
			$address = 'SELECT restaurant_name, address, city, province FROM all_menus WHERE location_id="' .$var1 .'" AND menu_type="' .$var2.'"'." GROUP BY restaurant_name";
			
			$result = $con->query($sql);
			$title = $con->query($address);
				
			if(!$result || !$title){
				die('Error2: '.mysql_error());
			}
			while($r = $title->fetch_array()){
				echo "<table border='1'>
					<caption>".$r['restaurant_name'].", address: ".$r['address'].", ".$r['city']." ".$r['province']." </caption>
					<tr>
						<th>Description</th>
						<th>Ordinary Price</th>
						<th>Vip Price</th>
					</tr></br>";		
		
		
				while($row = $result->fetch_array()){
					echo "<tr>";
					echo "<td>" .$row['description']."</td>";
					echo "<td>" .$row['ordinary_price']."</td>";
					echo "<td>" .$row['vip_price']."</td>";
					echo "</tr>";
				}
			}
		}elseif($var2 == 'restaurant_name'){
			$sql = 'SELECT description, ordinary_price, vip_price FROM all_menus WHERE '.$var2 .' ="' .$var1 .'"';
			$address = 'SELECT menu_type FROM all_menus WHERE '.$var2 .' ="' .$var1 .'"';
			
			
			$list = array("Main Course", "Children Menu", "Entree", "Wine", "Dessert");
			$i=0;
			while ($i != 5){
				$result = $con->query($sql .' AND menu_type="'.$list[$i].'"');
				$title = $con->query($address.' AND menu_type="'.$list[$i].'"' ." Group BY menu_type");

				if(!$result || !$title){
					die('Error2: '.mysql_error());
				}
				while($r = $title->fetch_array()){
					echo "<table border='1'>
						<caption>".$var1.": " .$r['menu_type'] ." </caption>
						<tr>
							<th>Description</th>
							<th>Ordinary Price</th>
							<th>Vip Price</th>
						</tr></br>";		
		
		
					while($row = $result->fetch_array()){
						echo "<tr>";
						echo "<td>" .$row['description']."</td>";
						echo "<td>" .$row['ordinary_price']."</td>";
						echo "<td>" .$row['vip_price']."</td>";
						echo "</tr>";
					}
				}
				$i=$i+1;
			
			}
			
			
			
		}elseif (!isset($var1) &&  !isset($var2)){
			$sql = 'SELECT description, ordinary_price, vip_price FROM all_menus';
			$address = 'SELECT restaurant_name, address, city, province FROM all_menus';
			
			$i=2;
			while ($i != 14){
				$result = $con->query($sql .' WHERE location_id='.$i);
				$title = $con->query($address .' WHERE location_id='.$i.' GROUP BY restaurant_name');
		
				if(!$result || !$title){
					die('Error2: '.mysql_error());
				}
				
				while($r = $title->fetch_array()){
					echo "<table border='1'>
					<caption>".$r['restaurant_name'].", address: ".$r['address'].", ".$r['city']." ".$r['province']." </caption>
					<tr>
						<th>Description</th>
						<th>Ordinary Price</th>
						<th>Vip Price</th>
					</tr></br>";		
		
		
					while($row = $result->fetch_array()){
						echo "<tr>";
						echo "<td>" .$row['description']."</td>";
						echo "<td>" .$row['ordinary_price']."</td>";
						echo "<td>" .$row['vip_price']."</td>";
						echo "</tr>";
					}
				}
				$i=$i+1;
			}
		}else{
			$sql = 'SELECT description, ordinary_price, vip_price, menu_type FROM all_menus WHERE '.$var2 .' ="' .$var1 .'"';
			$address = 'SELECT restaurant_name, address, city, province, menu_type FROM all_menus WHERE '.$var2 .' ="' .$var1 .'"';
		
			$i=2;
			while ($i != 13){
				$result = $con->query($sql .' AND location_id='.$i);
				$title = $con->query($address .' AND location_id='.$i.' GROUP BY restaurant_name');
		
				if(!$result || !$title){
					die('Error2: '.mysql_error());
				}
				
				while($r = $title->fetch_array()){
					echo "<table border='1'>
					<caption>".$r['restaurant_name'].", address: ".$r['address'].", ".$r['city']." ".$r['province']." </caption>
					<tr>
						<th>".$r['menu_type'] ."</th>
						<th>Ordinary Price</th>
						<th>Vip Price</th>
					</tr></br>";		
		
		
					while($row = $result->fetch_array()){
						echo "<tr>";
						echo "<td>" .$row['description']."</td>";
						echo "<td>" .$row['ordinary_price']."</td>";
						echo "<td>" .$row['vip_price']."</td>";
						echo "</tr>";
					}
				}
				$i=$i+1;
			}
		}
		
		$result->close();
		$con->close();
	}
?>