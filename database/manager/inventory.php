<?php 
ob_start();
session_start();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<title>Select Information</title>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<link rel="stylesheet" type="text/css" href="../style.css" />
</head>

<body>

	<div id="page-wrap">
				
		<div id="contact-area">
			
						<?php
							include ('../connection.php');
							
							$con = makeConnection();
							$loc = getLocation();
							
							$sql = 'SELECT * FROM all_inventory WHERE location_id =' .$loc ;
							
							$result = $con->query($sql);

							if(!$result){
								die('Error2: '.mysql_error());
							}
							echo "<table border='1'>
								<tr>
									<th>Inventory</th>
									<th>Quantity</th>		
									<th>Cost</th>	
									<th>Category</th>
									<th>Purchase Date</th>	
									<th>Expire Date</th>	
								</tr>";		
						while($row = $result->fetch_array()){
							echo "<tr>";
							echo "<td>" .$row['supply_name']."</td>";
							echo "<td>" .$row['quantity']."</td>";
							echo "<td>" .$row['cost']."</td>";
							echo "<td>" .$row['category']."</td>";
							echo "<td>" .$row['purchase_date']."</td>";
							echo "<td>" .$row['expire_date']."</td>";
							echo "</tr>";
						}
						$result->close();
						$con->close();
					
						?>

		</div>
	
	</div>

</body>

</html>